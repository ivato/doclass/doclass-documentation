# Doclass documentation

## Introduction

Doclass is a set of open-source software that aims to support document annotation and classification.

## Architecture

![Architecture](architecture.png)

Doclass architercture is based on three layers:
* Interface (Doclass Frontend) is responsible to interacte with user or other applications. It communicates with Doclass Backend consuming its REST API.
* Application (Doclass Backend) is reponsible to provides Web Services, with a REST API.
* Memory is responsible to store persistent and non-persistent data for the application: session, cache, dataset or application.

## Pipeline

## Doclass Mobile

[Doclass mobile](https://gitlab.com/ivato/doclass/doclass-mobile) is localized in interface layer and implemented with [React Native](https://reactnative.dev) and is compatible with Android and Ios. It is responsible for interacting with users on a mobile platform and communicating with the backend through web services, currently following the REST standard.

### Used technologies

* [React Native](https://reactnative.dev) is is an open-source mobile application framework  used to develop applications for Android and iOS.

## Doclass Backend

[Doclass backend](https://gitlab.com/ivato/doclass/doclass-backend) is localized in application layer and implemented with Python 3. It is responsible for semi-supervised machine learning and for the intermediation between the user interface and the memory layer.
* [Used technologies](https://gitlab.com/ivato/doclass/doclass-backend/-/blob/master/README.md#used-technologies)
* [How to test performance](https://gitlab.com/ivato/doclass/doclass-backend/-/blob/master/README.md#how-to-test-performance)

## Related work
|      Name:     | Active Learning: | Open Source: | Modular Architecture | Crowd-sourcing | Document Prediction: | Labeling: | Annotation: |                  Link:                 |
|:--------------:|------------------|--------------|----------------------|----------------|:--------------------:|-----------|-------------|:--------------------------------------:|
| brat           |        No        |      Yes     |          No          |       Yes      |          No          |     No    |     Yes     | https://brat.nlplab.org/index.html     |
| doccano        |        No        |      Yes     |          Yes         |       Yes      |          No          |    Yes    |     Yes     | https://doccano.github.io/doccano/     |
| Prodigy        |        Yes       |      Yes     |          Yes         |       No       |          No          |    Yes    |     Yes     | https://prodi.gy/                      |
| Monkey Learn   |        No        |      No      |          Yes         |       No       |          Yes         |    Yes    |     Yes     | https://monkeylearn.com/               |
| Dualist        |        Yes       |      Yes     |          Yes         |       No       |          Yes         |    Yes    |      No     | https://github.com/burrsettles/dualist |
| FeatureInsight |        No        |      No      |          No          |       No       |          Yes         |    Yes    |     Yes     | https://bit.ly/2EmhG6E                 |

## How to cite

Waiting publication...
..
